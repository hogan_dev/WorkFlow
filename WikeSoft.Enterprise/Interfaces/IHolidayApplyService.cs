﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Filters;


namespace WikeSoft.Enterprise.Interfaces
{
    public interface IHolidayApplyService
    {
        string Add(HolidayModel data);

        void Edit(HolidayModel data);


        HolidayModel Get(string id);

        void Delete(IList<string> ids);

        PagedResult<HolidayModel> GetList(HolidayFilter filter);
 
        bool StartWorkFlow(IList<string> ids, string userId);
        bool RejectWorkFlow(string ids);
        bool FinishWorkFlow(string id);
    }
}
