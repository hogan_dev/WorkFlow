﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Enterprise.Models.WorkFlow
{
    public class CertificateWorkFlow
    {
        ///<summary>
        /// 流程Id
        ///</summary>
        public string FlowId { get; set; } // FlowId (length: 50)
        public string NodeId { get; set; }

        
        [Display(Name = "审核意见")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string Message { get; set; }
        /// <summary>
        /// 是否通过，1：通过， 0：驳回
        /// </summary>
        public string Condition { get; set; }

        public Guid? CustomerCertificateId { get; set; }

        public Guid? CompanyCertificateId { get; set; }
        
    }
}
