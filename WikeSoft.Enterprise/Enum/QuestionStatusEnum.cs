﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 盖章情况
    /// </summary>
    public enum QuestionStatusEnum
    {
        /// <summary>
        /// 已标记
        /// </summary>
        [Description("已标记")]
        Marked = 0,

        /// <summary>
        /// 处理中
        /// </summary>
        [Description("处理中")]
        Handling = 1,

        /// <summary>
        /// 已处理
        /// </summary>
        [Description("已处理")]
        Handled = 2,

        /// <summary>
        /// 已解决
        /// </summary>
        [Description("已解决")]
        Finished = 3
    }
}
