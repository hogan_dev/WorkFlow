﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    public enum CustomerBackStatusEnum
    {
        /// <summary>
        /// 保存
        /// </summary>
        Save = 0,

        /// <summary>
        /// 流程中
        /// </summary>
        WorkFlow = 1,

        /// <summary>
        /// 已退
        /// </summary>
        Finish=2
    }
}
