﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 盖章情况
    /// </summary>
    public enum StampEnum
    {
        /// <summary>
        /// 未盖章
        /// </summary>
        [Description("未盖章")]
        No = 0,

        /// <summary>
        /// 已盖章
        /// </summary>
        [Description("已盖章")]
        Yes = 1
    }
}
