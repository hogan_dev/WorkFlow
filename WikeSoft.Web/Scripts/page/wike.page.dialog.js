﻿function openPostDialog() {
    top.layer.open({
        title: '功能查询',
        type: 2,
        content: "/Page/ListView",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var tem = $(layero).find("iframe")[0].contentWindow.getContent();
            setParentData(tem.Id, tem.PageName);
            top.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setParentData(id, pageName) {
    $("#ParentId").val(id);
    $("#ParentPageName").val(pageName);
}


$(document).ready(function () {
    $("#btnSelectPost").click(openPostDialog);

    $("#btnClear").click(function () {
        $("#ParentId").val("");
        $("#ParentPageName").val("");
    });
});