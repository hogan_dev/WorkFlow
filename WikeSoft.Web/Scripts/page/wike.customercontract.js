﻿function openAddPage() {
    
    top.layer.open({
        title: '证书选择',
        type: 2,
        content: "/CustomerCertificate/SelectCustomerCertificateView",
        area: ['1000px', '600px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
          
            var data = $(layero).find("iframe")[0].contentWindow.getContent();
            if (data.length == 0) {
                top.layer.alert("请选择数据");
                return;
            }
           
            var ids = jQuery("#table_list").jqGrid('getDataIDs');
            for (var c = 0; c < ids.length; c++) {
                var haveId = ids[c];
                for (var j = 0; j < data.length; j++) {
                    var addId = data[j].Id;
                    if (haveId == addId) {
                        top.layer.alert("不能重复添加证书" + data[j].CertificateName);

                        return;
                    }
                }
            }

            for (var i = 0; i < data.length; i++) {
                var row = data[i]; 
                row.CustomerCertificateId = row.Id;
                row.SinglePrice = row.GetAmount;
                $("#table_list").jqGrid("addRowData", row.CustomerCertificateId, row, "last");
            }
            top.layer.close(index);

        }, cancel: function () {
            return true;
        }
    });
}

function openEditPage() {
    var row = WikeGrid.GetData();
    if (row != null) {
        top.layer.open({
            title: '证书修改',
            type: 2,
            content: "/CustomerCertificate/Edit?id=" + row.Id,
            area: ['1000px', '550px']
        });
    } else {
        top.layer.alert("请选择要编辑的数据");
    }
}

function deleteData() {
    var delDatas = WikeGrid.GetDataTableDeleteData();
    if (delDatas.Len > 0) {
        var btn = $("#btnDelete");
        top.layer.confirm("确认要删除这" + delDatas.Len + "条数据？", {
            btn: ['确认', '取消'] //按钮
        }, function () {
            for (var i = 0; i < delDatas.Data.length ; i++) {
                $("#table_list").jqGrid("delRowData", delDatas.Data[i]);
            }
            top.layer.closeAll();
        }, function () {
            btn.button('reset');
        });
    } else {
        top.layer.alert("请选择要删除的数据！");
    }
}