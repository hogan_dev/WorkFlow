﻿
$(document).ready(function () {
    $("#SType").change(function () {
       
    });

    $("#btnSelectPost").click(openPostDialog);

    $("#btnClear").click(function() {
        $("#PostId").val("");
        $("#PostName").val("");
    });
});

function openPostDialog() {
    parent.layer.open({
        title: '岗位查询',
        type: 2,
        content: "/Post/QueryPosts",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var tem = $(layero).find("iframe")[0].contentWindow.getContent();
            setPostData(tem.Id, tem.PostName);
            parent.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setPostData(postId, postName) {
    $("#PostId").val(postId);
    $("#PostName").val(postName);
}
