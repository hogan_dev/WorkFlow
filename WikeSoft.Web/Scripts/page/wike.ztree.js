﻿var WikeTree = function (config) {
    this.id = config.id;
    this.setting = {
        view: {
            selectedMulti: false
        },
        async: {
            enable: true,
            url: config.loadUrl,
            autoParam: ["id", "name=n", "level=lv"]
        },
        callback: {
            beforeClick: config.beforeClick
        }
    };

    this.addPage = function (e) {
     
        parent.layer.open({
            title: '添加',
            type: 2,
            content: config.addUrl + '/' + $("#txtParentId").val(),
            area: ['800px', '550px'],
            end: function () {
               
                var zTree = $.fn.zTree.getZTreeObj(e.data.scope.id),
                    type = "refresh",
                    nodes = zTree.getSelectedNodes();
                if (nodes.length === 0) {
                    zTree.reAsyncChildNodes(null, type, false);
                    return;
                }
                var node = nodes[0];
                node.isParent = true;
                zTree.updateNode(node, false);
                zTree.reAsyncChildNodes(node, type, false);
            }
        });
    }

    this.editPage = function(e) {
        var id = $("#txtParentId").val();
        if (id === "") {
            parent.layer.alert("请选择要编辑的节点");
            return;
        }
        parent.layer.open({
            title: '编辑',
            type: 2,
            content: config.editUrl + '/' + id,
            area: ['800px', '550px'],
            end: function () {
                var zTree = $.fn.zTree.getZTreeObj(e.data.scope.id),
                    type = "refresh",
                    nodes = zTree.getSelectedNodes();
                if (nodes.length === 0) {
                    return;
                }
                var node = nodes[0];
                zTree.reAsyncChildNodes(node.getParentNode(), type, false);
            }
        });
    }

    this.deletePage = function(e) {
        var id = $("#txtParentId").val();
        if (id === "") {
            parent.layer.alert("请选择要删除的页面");
            return;
        }
        var scope = e.data.scope;
        var onDeleted = scope.onDeleted;
        parent.layer.confirm('确定删除吗?',
            { icon: 3, title: '提示' },
            function(index) {
                //loading
                parent.layer.load(2, { shade: 0.3 });
                $.post(config.deleteUrl,
                    { id: id },
                    function(res) {
                        parent.layer.close(index);
                        parent.layer.closeAll();
                        if (res && res.success) {
                            parent.layer.alert("删除成功");
                            onDeleted.call(scope, null);
                        } else {
                            parent.layer.alert(res.message);
                        }
                    });
            });
    }
    //删除本地的节点
    this.onDeleted = function () {
        var zTree = $.fn.zTree.getZTreeObj(this.id),
            nodes = zTree.getSelectedNodes();
        if (nodes.length === 0) {
            return;
        }
        zTree.removeNode(nodes[0], null);
    }
};

WikeTree.prototype.load = function () {
    
    $.fn.zTree.init($("#" + this.id), this.setting);
    var data = {
        scope: this
    };
    $("#btnAdd").bind("click", data, this.addPage);
    $("#btnEdit").bind("click", data, this.editPage);
    $("#btnDelete").bind("click", data, this.deletePage);
}