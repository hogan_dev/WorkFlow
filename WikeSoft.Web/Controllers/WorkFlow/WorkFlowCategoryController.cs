﻿using System.Web.Mvc;
using WikeSoft.Core.Extension;
using WikeSoft.WorkFlowEngine.Enum;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Interfaces;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.Web.Controllers.WorkFlow
{
    public class WorkFlowCategoryController : BaseController
    {
        private readonly IWorkFlowCategoryService _workFlowCategoryService;
     

       
       
        public WorkFlowCategoryController( IWorkFlowCategoryService workFlowCategoryService)
        {
            _workFlowCategoryService = workFlowCategoryService;
          
        }

        public ActionResult ListView()
        {
            return View();
        }

        // GET: Page

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="id">父页面ID</param>
        /// <returns></returns>

        public ActionResult Add(string id)
        {
            WorkFlowCategoryModel parent = null;
            if (id.IsNotBlank())
            {
                parent = _workFlowCategoryService.Find(id);
            }
            var model = new WorkFlowCategoryModel();
            if (parent != null)
            {
                model.ParentId = parent.Id;
                model.ParentName = parent.CategoryName;
            }
           
            return View(model);
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(WorkFlowCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _workFlowCategoryService.Add(model);
                return PartialView("CloseLayerPartial");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        public ActionResult Edit(string id)
        {
            var model = _workFlowCategoryService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(WorkFlowCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _workFlowCategoryService.Edit(model);
                
                return PartialView("CloseLayerPartial");
            }
            return View(model);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string id)
        {
            var message = _workFlowCategoryService.Delete(id);


            if (message.Code == CodeEum.Success)
            {
                return Ok();
            }
            return Fail(message.Message);
        }

        /// <summary>
        /// 根据父节点Id获取树
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetTrees(string id)
        {
            //var parentId =  Guid.Empty.ToString().ToUpper();
            var pages = _workFlowCategoryService.GetByParentId(id);
            return JsonOk(pages);
        }

        [HttpGet]
        public JsonResult Query(WorkFlowCategoryFilter filter)
        {
            var result = _workFlowCategoryService.Query(filter);
            return JsonOk(result);
        }

        public ActionResult HistoryListView()
        {
            return View();
        }
    }
}