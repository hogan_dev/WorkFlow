﻿using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using WikeSoft.WorkFlowEngine.Interfaces;

namespace WikeSoft.Web.Controllers.WorkFlow
{
    public class WorkFlowController : BaseController
    {
        private readonly IWorkFlowInstanceService _flowInstanceService;
        public WorkFlowController()
        {
            _flowInstanceService = DependencyResolver.Current.GetService<IWorkFlowInstanceService>();
        }
        // GET: WorkFlow
        public void Pic(string flowId)
        {
            var image = _flowInstanceService.GetRunBitmap(flowId);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            HttpContext.Response.Clear();
            HttpContext.Response.ContentType = "image/jpeg";
            HttpContext.Response.BinaryWrite(stream.ToArray());

        }
    }
}