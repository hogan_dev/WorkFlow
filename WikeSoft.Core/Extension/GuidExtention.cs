﻿using System;

namespace WikeSoft.Core.Extension
{
    /// <summary>
    /// Guid  扩展
    /// </summary>
    public static class GuidExtention
    {

        /// <summary>
        /// 为null，或者Guid.Empty
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty(this Guid? value)
        {
            return !value.HasValue || value.Value == Guid.Empty;
        }

        /// <summary>
        /// 不为null，并且Guid.Empty
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNotEmpty(this Guid? value)
        {
            return !value.IsEmpty();
        }

    }
}
